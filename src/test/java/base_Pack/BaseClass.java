package base_Pack;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeSuite;

import util_Pack.BrowserFactory;
import util_Pack.ConfigDataProvider;
import util_Pack.ExcelDataProvider;
 
public class BaseClass    {

	public WebDriver driver;
	public ConfigDataProvider config;
	public ExcelDataProvider excel;
	public int rowCount;


	public void setUp1() {

		driver=BrowserFactory.startBrowser(driver, config.getBrowser(), config.getQaURL());
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}
	
	public void setUp2() {

		 excel=new ExcelDataProvider();
		 rowCount= excel.rowcountNumber();
		driver=BrowserFactory.startBrowser(driver, config.getBrowser(), config.getQaURL());
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

	}

	@BeforeSuite
	public void setUpSuite() {

		config=new ConfigDataProvider(); 

	}

}
