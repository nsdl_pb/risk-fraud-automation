package util_Pack;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.util.Date;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base_Pack.BaseClass;

public class Demo extends BaseClass {
	
	@BeforeClass
	public void setup() throws InterruptedException {
		setUp1();
	}
	 
	@Test
	public void downloadFileFromNFS() throws InterruptedException {
		login();
		selectfile();
		selectdate();
		logout();
		
	}
	
	public void login() throws InterruptedException {
		driver.navigate().to("https://prepaid.nsdlbank.co.in/GeniusCCPrepaid/");
		driver.findElement(By.xpath("//*[@id=\"userId\"]")).sendKeys("PPISUPPORT");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("Nsdl@1234");
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"loginForm\"]/table[2]/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[2]/td/table/tbody/tr/td[2]/table/tbody/tr[6]/td[3]/table/tbody/tr[1]/td[2]/a/img")).click();
		Thread.sleep(4000);
	}
		

	public void selectfile() throws InterruptedException {
		driver.switchTo().frame("showframeLeft");
//		WebDriverWait wait =new WebDriverWait(driver, 40);	
//		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='GeniusSolutionSuite']"))); 
		driver.findElement(By.xpath("//*[text()='GeniusSolutionSuite']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Prepaid Card']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Reports']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Ad hoc Reports']")).click();
		Thread.sleep(10000);
		driver.switchTo().defaultContent();
	}
	public void selectdate() throws InterruptedException {
		Thread.sleep(2000);
		driver.switchTo().frame("showframe");
		Thread.sleep(2000);
		driver.switchTo().frame("rtop");
		Thread.sleep(2000);
		Thread.sleep(2000);
		Select s=new Select(driver.findElement(By.xpath("//*[@id=\"reportId\"]")));
		s.selectByVisibleText("SETTLED TRANSACTION REPORT");
		Thread.sleep(1000);
		
		Thread.sleep(1000);
		WebElement datePicker = driver.findElement(By.xpath("//input[contains(@class, 'hasDatepicker')]"));
		datePicker.click();
	
		LocalDate currentDate = LocalDate.now();
		LocalDate previousDate = currentDate.minusDays(1);
		
		 int day = previousDate.getDayOfMonth();
		 String sday=Integer.toString(day);
		 
		int month = previousDate.getMonthValue();
		int accMonth=month-1;
		String smonth=Integer.toString(accMonth);
		
		int year = previousDate.getYear();
		String syear=Integer.toString(year);
		
		Thread.sleep(2000);
		Select s1=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]")));
		s1.selectByValue(smonth);
		Thread.sleep(1000);
		
		Thread.sleep(2000);
		Select s2=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]")));
		s2.selectByValue(syear);
		Thread.sleep(1000);
		
		Thread.sleep(1000);
		Actions ac = new Actions(driver);
		WebElement live= driver.findElement(By.xpath("//a[text()='"+sday+"']"));
		ac.moveToElement(live).click().build().perform();
		Thread.sleep(1000);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"todate\"]")).click();
		Thread.sleep(1000);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//a[@href='#' and @class='ui-state-default ui-state-highlight ui-state-hover']")).click();
		Thread.sleep(1000);
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[@id=\"close\"]")).click();
		
		WebDriverWait wait =new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='Download Report']"))); 
		System.out.println("Report display sucessfully");
		
		Thread.sleep(2000);
		driver.findElement(By.xpath("//*[text()='Download Report']")).click();
		Thread.sleep(10000);
		
		driver.switchTo().defaultContent();
		}
	
	

		public void logout() throws InterruptedException {
			Thread.sleep(3000);
			driver.switchTo().frame(driver.findElement(By.xpath("/html/frameset/frame[1]")));
			Thread.sleep(2000);
			driver.findElement(By.xpath("//*[text()='Logout']")).click();
			Thread.sleep(2000);
		}
		
	}

