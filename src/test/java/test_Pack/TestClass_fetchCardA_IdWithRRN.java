package test_Pack;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base_Pack.BaseClass;
import pom_Pack.RiskT_ProofDownload;
import pom_Pack.RiskT_mapRRN_ID;

public class TestClass_fetchCardA_IdWithRRN extends BaseClass {

	public RiskT_mapRRN_ID b1;

	@BeforeClass
	public void setup() throws InterruptedException {
		setUp1(); 
		b1=new RiskT_mapRRN_ID(driver);
	}

	@Test
	public void RiskTeamAutomation() throws InterruptedException {

		b1.chromeSetup();
		b1.loginToAEPS(config.getDataFromConfig("ID"),config.getDataFromConfig("Password"));
        b1.selctTransactionTab();
    try {    for(int i=1;i<=rowCount;i++) {
			if(!excel.getStringData(i,7).isEmpty()) {
					if(excel.getStringData( i, 3).equalsIgnoreCase("Fraud Chargeback")|| excel.getStringData( i, 3).equalsIgnoreCase("Good Faith Fraud Chargeback")|| excel.getStringData( i, 3).equalsIgnoreCase("Fraud Complaint Re-raise")) {			
			    b1.fetchDatafromWeb(excel.getStringData( i, 7),excel.getStringData( i, 9));
					}	}else {
				break;
			}	
		}
    }catch (Exception e) {
    	System.out.println(e.getCause());
		System.err.println("Fraud Chargeback, Good Faith Fraud Chargeback and Good Faith Fraud Chargeback attachment Not present");
	}
        
	}
	
	@AfterClass
	public void logoutFromAEPS_RT() throws InterruptedException {
		b1.AepsRisk_Logout();
	}
	
}
