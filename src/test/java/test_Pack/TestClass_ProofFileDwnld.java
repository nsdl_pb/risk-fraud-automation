package test_Pack;

import java.io.File;
import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base_Pack.BaseClass;
import pom_Pack.RiskT_ProofDownload;
import pom_Pack.RiskT_mapRRN_ID;

public class TestClass_ProofFileDwnld extends BaseClass {

	public RiskT_ProofDownload b1;
	public RiskT_mapRRN_ID b2;

	
	 
	@BeforeClass
	public void setup() throws InterruptedException, IOException {
		setUp2(); 
		b1=new RiskT_ProofDownload(driver);
		b2=new RiskT_mapRRN_ID(driver);
	}

	@Test
	public void RiskTeamAutomation() throws InterruptedException {

		b1.chromeSetup();
		b1.loginToAEPS(config.getDataFromConfig("ID"),config.getDataFromConfig("Password"));
		b1.selctTransactionTab();
		try {    for(int i=1;i<=rowCount;i++) {

			if(!excel.getStringData(i,7).isEmpty()) {
				if(excel.getStringData( i, 3).equalsIgnoreCase("Fraud Chargeback")|| excel.getStringData( i, 3).equalsIgnoreCase("Good Faith Fraud Chargeback")|| excel.getStringData( i, 3).equalsIgnoreCase("Fraud Complaint Re-raise")) {			
					b2.fetchDatafromWeb(excel.getStringData( i, 7),excel.getStringData( i, 9));
					if(!excel.getStringData( i, 27).isEmpty()) {
						b2.downloadEvidanceWithRRN();
					}else {b2.backOperation();	}
				}}  
		}
		
		}catch (Exception e) {
			System.out.println(e);
			System.err.println("Risk Automation Script fail");
		}
	}

	
	@AfterClass
	public void logoutFromAEPS_RT() throws InterruptedException {
		b1.AepsRisk_Logout();
	}
}
