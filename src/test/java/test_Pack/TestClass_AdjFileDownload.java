package test_Pack;

import java.io.IOException;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base_Pack.BaseClass;
import pom_Pack.RiskT_AdjRepoDownload;
 
public class TestClass_AdjFileDownload extends BaseClass {

	public RiskT_AdjRepoDownload a1;

	@BeforeClass
	public void setup() throws InterruptedException {
		setUp1();
		a1=new RiskT_AdjRepoDownload(driver);
	}
	@Test
	public void fraudAdjustmentRepoDownload() throws InterruptedException, IOException {

		a1.chromeSetup();
		a1.loginToAEPS(config.getDataFromConfig("ID"),config.getDataFromConfig("Password"));
		a1.downloadAdjustmentReoprt(config.getDataFromConfig("cycleName"), config.getDataFromConfig("adjestmentfile"), config.getDataFromConfig("fileFormat"));
	}
	

	
	@AfterClass
	public void logoutFromAEPS() throws InterruptedException, IOException {
		a1.AepsRisk_Logout();
		driver.quit();
		
		 
	}
	
}
