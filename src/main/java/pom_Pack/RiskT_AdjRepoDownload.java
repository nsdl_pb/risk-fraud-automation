package pom_Pack;


import java.time.LocalDate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class RiskT_AdjRepoDownload {


	public WebDriver driver;

	public RiskT_AdjRepoDownload(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}

	@FindBy(xpath="//*[@id='details-button']")
	private WebElement chromeAdvanceBtn;

	@FindBy(xpath="//*[@id='proceed-link']")
	private WebElement chromeProccedLink;	

	@FindBy(xpath="//*[@id='loginIDText']")
	private WebElement loginIDTextField;	

	@FindBy(xpath="//*[@id='passwordText']")
	private WebElement passwordTextField;	

	@FindBy(xpath="//*[@id='loginBtn']")
	private WebElement loginBtn;

	@FindBy(xpath="//*[@id='forgtpwd']")
	private WebElement forgotPassBtn;

	@FindBy(xpath="//*[@id='Transaction Search']/a")
	private WebElement transactionSearchTab;	

	@FindBy(xpath="//*[text()='View Transaction']")
	private WebElement viewTransactionOption;	

	@FindBy(xpath="//*[@id='effrom']")
	private WebElement calenderField;	

	@FindBy(xpath="//a[@href='#' and @class='ui-state-default ui-state-highlight ui-state-hover']")
	private WebElement highlighedDate;	

	@FindBy(xpath="/html/body/div[1]/header/div/div[2]/ul/li/a")
	private WebElement profileMenu;	

	@FindBy(xpath="//a[text()='Logout']")
	private WebElement logoutOption;	

	@FindBy(xpath="//*[@id='Reports']/a")
	private WebElement reportsTab;	

	@FindBy(xpath="//a[text()='Adjustment Reports']")
	private WebElement adjestmentReport;	

	@FindBy(xpath="//select[@id='adjflag']")
	private WebElement adjusmentTypeDD;	

	@FindBy(xpath="//select[@id='fileFormat']")
	private WebElement fileFormatDD;	

	@FindBy(xpath="//*[@id='searchBtn']")
	private WebElement downloadReport;	

	
	public void chromeSetup() throws InterruptedException {

		Thread.sleep(2000);
		chromeAdvanceBtn.click();
		Thread.sleep(2000);
		chromeProccedLink.click();
		Thread.sleep(2000);

	}

	public void loginToAEPS(String ID, String Pass) throws InterruptedException {

		Thread.sleep(2000);
		loginIDTextField.sendKeys(ID);
		Thread.sleep(2000);
		passwordTextField.sendKeys(Pass);
		Thread.sleep(2000);
		loginBtn.click();
		Thread.sleep(3000);

	}

	public void selctFromdate() throws InterruptedException {
		Thread.sleep(2000);
		WebElement datePicker = driver.findElement(By.xpath("//*[@id='effrom']"));
		datePicker.click();
		selectdateForAdjustmentR();
	}
	
public void selectTodate() throws InterruptedException {
	Thread.sleep(2000);
	WebElement datePicker = driver.findElement(By.xpath("//*[@id='efto']"));
	datePicker.click();
	selectdateForAdjustmentR();
	}

	public void selectdateForAdjustmentR() throws InterruptedException {

		Thread.sleep(1000);
		LocalDate currentDate = LocalDate.now();
		LocalDate previousDate = currentDate.minusDays(1);

		int day = previousDate.getDayOfMonth();
		String sday=Integer.toString(day);

		int month = previousDate.getMonthValue();
		int accMonth=month-1;
		String smonth=Integer.toString(accMonth);

		int year = previousDate.getYear();
		String syear=Integer.toString(year);

		Thread.sleep(2000);
		Select s1=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]")));
		s1.selectByValue(smonth);
		Thread.sleep(1000);

		Thread.sleep(2000);
		Select s2=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]")));
		s2.selectByValue(syear);
		Thread.sleep(1000);

		Thread.sleep(1000);
		Actions ac = new Actions(driver);
		WebElement live= driver.findElement(By.xpath("//a[text()='"+sday+"']"));
		ac.moveToElement(live).click().build().perform();
		Thread.sleep(1000);

	}

	public void downloadAdjustmentReoprt(String cyclename,String adjestmentfile, String fileFormat) throws InterruptedException {

		Thread.sleep(1000);
		reportsTab.click();
		Thread.sleep(2000);
		adjestmentReport.click();
		Thread.sleep(2000);
		selctFromdate();
		selectTodate();
		Thread.sleep(2000);

		Select s1= new Select(adjusmentTypeDD) ;
		s1.selectByVisibleText(adjestmentfile);
		Thread.sleep(1000);
		System.out.println("\""+adjestmentfile+"\""+"selected from dropdown");

		Thread.sleep(1000); 
		Select s=new Select(driver.findElement(By.xpath("//select[@id='cyclename']")));
		s.selectByVisibleText(cyclename);
		System.out.println("Cycle name ="+cyclename+" selected from dropdown");
		Thread.sleep(2000);

		Select s2= new Select(fileFormatDD) ;
		s2.selectByVisibleText(fileFormat);
		Thread.sleep(2000);
		System.out.println("\""+fileFormat+"\""+"selected from dropdown");

		Thread.sleep(2000);
		downloadReport.click();
		Thread.sleep(10000);

	}

	public void AepsRisk_Logout() throws InterruptedException {

		profileMenu.click();
		Thread.sleep(2000);
		logoutOption.click();
		Thread.sleep(2000);
		System.out.println("IMPS Web-Application logout sucessfully");

	}

}
