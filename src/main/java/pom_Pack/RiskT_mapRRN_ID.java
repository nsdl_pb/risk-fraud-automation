package pom_Pack;


import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import xlutil_Pack.XLUtility;

public class RiskT_mapRRN_ID  extends XLUtility{


	public WebDriver driver;
	public XLUtility xlutil;
	public static int r=1;

	public RiskT_mapRRN_ID(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver,this);

		xlutil=new XLUtility();
	}

	@FindBy(xpath="//*[@id='details-button']")
	private WebElement chromeAdvanceBtn;

	@FindBy(xpath="//*[@id='proceed-link']")
	private WebElement chromeProccedLink;	

	@FindBy(xpath="//*[@id='loginIDText']")
	private WebElement loginIDTextField;	

	@FindBy(xpath="//*[@id='passwordText']")
	private WebElement passwordTextField;	

	@FindBy(xpath="//*[@id='loginBtn']")
	private WebElement loginBtn;

	@FindBy(xpath="//*[@id='forgtpwd']")
	private WebElement forgotPassBtn;

	@FindBy(xpath="//*[@id='Transaction Search']/a")
	private WebElement transactionSearchTab;	

	@FindBy(xpath="//*[text()='View Transaction']")
	private WebElement viewTransactionOption;	

	@FindBy(xpath="//*[@id='effrom']")
	private WebElement calenderField;	

	@FindBy(xpath="//a[@href='#' and @class='ui-state-default ui-state-highlight ui-state-hover']")
	private WebElement highlighedDate;	

	@FindBy(xpath="/html/body/div[1]/header/div/div[2]/ul/li/a")
	private WebElement profileMenu;	

	@FindBy(xpath="//a[text()='Logout']")
	private WebElement logoutOption;	

	@FindBy(xpath="//*[@id='pgstatus']/div")
	private WebElement recordNotFound;	

	@FindBy(xpath="//*[@id='transactionDate']")
	private WebElement transactiondateF;	

	@FindBy(xpath="//*[@id='rrnNo']")
	private WebElement rrnNoField;	

	@FindBy(xpath="//*[@id='searchButon']")
	private WebElement searchBtn1;	

	@FindBy(xpath="//*[text()='View']")
	private WebElement viewBtn;

	@FindBy(xpath="//*[text()=' Good Faith Fraud Chargeback  ']")
	private WebElement goodFaith123;

	@FindBy(xpath="//*[@style='color: red' and @class='dwnldEvidence para']")
	private WebElement evidenceDownloadBtn;

	@FindBy(xpath="//*[text()='Back']")
	private WebElement backBtn;


	public void chromeSetup() throws InterruptedException {

		Thread.sleep(2000);
		chromeAdvanceBtn.click();
		Thread.sleep(2000);
		chromeProccedLink.click();
		Thread.sleep(2000);

	}

	public void loginToAEPS(String ID, String Pass) throws InterruptedException {

		Thread.sleep(2000);
		loginIDTextField.sendKeys(ID);
		Thread.sleep(2000);
		passwordTextField.sendKeys(Pass);
		Thread.sleep(2000);
		loginBtn.click();
		Thread.sleep(3000);

	}



	public void selectdateForTransactionSearch(String rrnDate) throws InterruptedException {

		String aaaa;

		Thread.sleep(1000);
		transactiondateF.click();
		Thread.sleep(1000);
		String abc=rrnDate;
		String sday=abc.substring(0, 2);
		if(sday.startsWith("0")) {

			aaaa=sday.replaceAll("0", "");
			System.out.println(aaaa.length());
		}else {
			aaaa=sday;
		}
		String month=abc.substring(3, 5);
		System.out.println(month);
		int i=Integer.parseInt(month);
		int qq=(i-1);
		String smonth=Integer.toString(qq);
		System.out.println(smonth);
		String syear=abc.substring(6, 10);
		System.out.println(syear);


		Thread.sleep(1000);
		Select s1=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]")));
		s1.selectByValue(smonth);
		Thread.sleep(1000);

		//	Thread.sleep(1000);
		Select s2=new Select(driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]")));
		s2.selectByValue(syear);
		Thread.sleep(1000);

		//		Thread.sleep(1000);
		Actions ac = new Actions(driver);
		WebElement live= driver.findElement(By.xpath("//a[text()='"+aaaa+"']"));
		ac.moveToElement(live).click().build().perform();
		Thread.sleep(1000);

	}

	public void selctTransactionTab() throws InterruptedException {
		Thread.sleep(1000);
		transactionSearchTab.click();
		Thread.sleep(2000);
		viewTransactionOption.click();
		Thread.sleep(2000);

		WebDriverWait wait =new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='transactionDate']"))); 
		Thread.sleep(1000);

		System.out.println("Transaction screen display");

	}

	public void downloadEvidanceWithRRN() throws InterruptedException {

		Thread.sleep(1000);
		moveToProofOption();
		selctTransactionLifeCycleLstOpt();
	}

	public void fetchDatafromWeb(String rrnDate,String rrnNo) throws InterruptedException, IOException {

		Thread.sleep(1000);
		selectdateForTransactionSearch(rrnDate);
		Thread.sleep(1000);

		rrnNoField.sendKeys(rrnNo);
		Thread.sleep(1000);
		searchBtn1.click();
		Thread.sleep(1000);

		WebDriverWait wait =new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()='View']"))); 

		Thread.sleep(1000);
		viewBtn.click();
		Thread.sleep(1000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[text()=' Download Transaction Details']"))); 
		Thread.sleep(1000); 
		String RRNno_Title=driver.findElement(By.xpath("//strong[text()='RRN']")).getText();
		//	Thread.sleep(1000); 
		String RRNno_data=driver.findElement(By.xpath("//td[@class='rrn']")).getText();
		//	Thread.sleep(1000); 

		System.out.println(RRNno_Title+"-- "+RRNno_data);

		String cardAccepterID_Title=driver.findElement(By.xpath("//strong[text()='Card Acceptor ID Code']")).getText();
		//	Thread.sleep(1000); 
		String cardAccepterID_data=driver.findElement(By.xpath("//*[@id=\"searchForm\"]/div[1]/div[2]/div/div/table/tbody/tr[10]/td[2]")).getText();
		//	Thread.sleep(1000); 

		System.out.println(cardAccepterID_Title+"-- "+cardAccepterID_data);

		if(r==1) {

			xlutil.setCellData("AutomationSheet", 0,0, RRNno_Title );
			xlutil.setCellData("AutomationSheet", 0,1, cardAccepterID_Title);
			xlutil.setCellData("AutomationSheet", 0,2, "EvidenceFile" );

		}

		xlutil.setCellData("AutomationSheet", r,0, RRNno_data );
		xlutil.setCellData("AutomationSheet", r,1, cardAccepterID_data);

	}


	public void moveToProofOption() throws InterruptedException {
		Thread.sleep(1000);
		JavascriptExecutor je1 = (JavascriptExecutor) driver;
		WebElement element11 = driver.findElement(By.xpath("//*[text()='Transaction Life Cycle']"));
		je1.executeScript("arguments[0].scrollIntoView(true);",element11);
		Thread.sleep(2000);
		System.out.println("Transaction Life Cycle header display sucessfully");
	}

	public void selctTransactionLifeCycleLstOpt() throws InterruptedException {

		Thread.sleep(1000);
		try {
			try{
				if(driver.findElement(By.xpath("//*[text()=' Fraud Complaint Re-raise  ']")).isDisplayed()) {
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[text()=' Fraud Complaint Re-raise  ']")).click();
					Thread.sleep(1000);
					abcd();
				}
			}catch (Exception e) {
				if(driver.findElement(By.xpath("//*[text()=' Fraud Chargeback  ']")).isDisplayed()) {
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[text()=' Fraud Chargeback  ']")).click();
					Thread.sleep(1000);
					abcd();
				}
			}}catch (Exception e) {
				if(driver.findElement(By.xpath("//*[text()=' Good Faith Fraud Chargeback  ']")).isDisplayed()) {
					Thread.sleep(1000);
					driver.findElement(By.xpath("//*[text()=' Good Faith Fraud Chargeback  ']")).click();
					Thread.sleep(1000);
					abcd();
				}
			}


	}


	public void abcd() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor je1 = (JavascriptExecutor) driver;
		WebElement element112 = driver.findElement(By.xpath("//*[text()='Download Evidence ']"));
		je1.executeScript("arguments[0].scrollIntoView(true);",element112);	
		System.out.println("scroll upto evidance........");



		try {	try {
			Thread.sleep(1000);
			//		List <WebElement> downloadBtns=driver.findElements(By.xpath("//*[contains(text(),'.jpeg')] | //*[contains(text(),'.xls')] | //*[contains(text(),'.pdf')] | //*[contains(text(),'.docx')] | //*[contains(text(),'.jpg')]"));
			List <WebElement> downloadBtns=driver.findElements(By.xpath("//td[@colspan='3']/a"));

			//		String proofFileName=driver.findElement(By.xpath("//td[@colspan='3']")).getText();

			int x=downloadBtns.size();                                  
			//	System.out.println("size of red are"+x);
			String finalName = "";

			for(int q=0;q<x;q++) {
				boolean asd=	downloadBtns.get(q).isDisplayed();

				if(asd==true) {

					if(finalName.isEmpty()) {
						finalName = downloadBtns.get(q).getText();
						xlutil.setCellData("AutomationSheet", r, 2, finalName );			
						downloadBtns.get(q).click();
						Thread.sleep(2000);
					}else {
						finalName = finalName + " | "+ downloadBtns.get(q).getText();
						xlutil.setCellData("AutomationSheet", r, 2, finalName );			
						downloadBtns.get(q).click();
						Thread.sleep(2000);
					}

				}
			}}catch (Exception e) {
				System.out.println(e.getMessage());
			}
		} finally {
			backOperation();
		} 
	}

	public void backOperation() throws InterruptedException {
		Thread.sleep(1000);
		WebElement element1121 = driver.findElement(By.xpath("//*[text()='Back']"));
		JavascriptExecutor je1 = (JavascriptExecutor) driver;
		je1.executeScript("arguments[0].scrollIntoView(true);",element1121);
		Thread.sleep(2000);
		r++;
		backBtn.click();
		Thread.sleep(1000);
		WebDriverWait wait =new WebDriverWait(driver, 60);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='transactionDate']"))); 
		Thread.sleep(1000);
	}


	public void AepsRisk_Logout() throws InterruptedException {

		profileMenu.click();
		Thread.sleep(2000);
		logoutOption.click();
		Thread.sleep(2000);
		System.out.println("IMPS Web-Application logout sucessfully");

	}

}
