package xlutil_Pack;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XLUtility {

	XSSFWorkbook workbook; 
	public String sheetnameFromIndex;
	public XSSFSheet sheet;
	public String path;
	public FileInputStream	fis;
	public XSSFRow row;
	public XSSFCell cell;
	public FileOutputStream fo;


	public XLUtility() {
	//	path="C:/Users/SixSigma_PrasadK/Desktop/IMP/RiskReportData.xlsx";
		path="C:\\Users\\SixSigma_PrasadK\\Downloads\\AepsAdjustmentReport.xlsx";

		File file =new File(path);
		try {
			fis = new FileInputStream(file);
			workbook=new XSSFWorkbook(fis);
			sheetnameFromIndex=	workbook.getSheetName(0);
			sheet= workbook.getSheet(sheetnameFromIndex);

			System.out.println(sheetnameFromIndex);
		} catch (Exception e) {
			System.out.println("Unable to read Excel File"+e.getMessage());
		}
	}

	public int rowcountNumber() {
		int rowcount=sheet.getLastRowNum();
		System.out.println(rowcount);
		return rowcount;
	}

	

	public void setCellData(String sheetName,int rownum, int colnum, String data) throws IOException{


		fis=new FileInputStream(path);
		workbook=new XSSFWorkbook(fis);

		if ( workbook.getSheetIndex(sheetName)==-1) {
			workbook.createSheet (sheetName) ;
		}

		sheet=workbook.getSheet(sheetName) ; 

		if(sheet.getRow(rownum)==null){
			sheet.createRow(rownum);
		}
		
		 row=sheet.getRow(rownum);
		
		 cell=row.createCell(colnum) ;
		cell.setCellValue(data);
		 fo=new FileOutputStream(path) ;
		workbook .write(fo) ;
		workbook. close();
		fis.close();
		fo.close();
	}


public String getStringData(int row,int a) {
	System.out.println();
	return workbook.getSheet(sheetnameFromIndex).getRow(row).getCell(a).getStringCellValue();
}


public double getNumericData(int row,int coloum) {
	return workbook.getSheet(sheetnameFromIndex).getRow(row).getCell(coloum).getNumericCellValue();
}

public Date getDateData(String sheetName,int row,int coloum) {
	return workbook.getSheet(sheetName).getRow(row).getCell(coloum).getDateCellValue();
}







}
